# STEAM models - Circuits models

This folder contains the input files for the Circuits models of the STEAM Library and a circuit parameters
directory with csv files of global parameters for each circuit family.

The input directory for each circuit has:
1. A csv file with coil resistances (used for interpolation and stimulus file generation)
2. A yaml file with General parameters, Global parameters, Netlist, Analysis, PostProcess and other Options.

Schematics of circuits:
RQX:
![img.png](schematics/RQX.png)