# MQXB reference model

(Copyright © 2022, CERN, Switzerland. All rights reserved.)

# Electrical order
![MQXB_electricalOrder](documents/MQXB_electricalOrder.png)

# Quench heater position
![MQXB_QH_position](documents/MQXB_QH_position.png)
