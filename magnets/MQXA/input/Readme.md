# MQXB reference model

(Copyright © 2022, CERN, Switzerland. All rights reserved.)

# Electrical order not known for MQXA

# Quench heater position
![MQXA_QH_position](documents/MQXA_QH_positions.png)

# Quench heater parameters
![MQXA_QH_parameters](documents/MQXA_QH_parameters.png)
