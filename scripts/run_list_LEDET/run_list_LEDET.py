from steam_sdk.drivers.DriverLEDET import DriverLEDET
from steam_sdk.parsers.ParserMat import get_signals_from_mat
import shutil
import random
import os
import warnings
import time


if __name__ == "__main__":
    path_folder_LEDET = 'C:\\tempLEDET\LEDET'
    # path_folder_LEDET = '\\\\eosproject-smb\eos\project\s\steam\steam_sdk_data\LEDET'
    dLEDET = DriverLEDET(
        path_exe         ='\\\\eosproject-smb\eos\project\s\steam\sw\\releases\ledet\Windows\LEDET_v2_02_26.exe',
        path_folder_LEDET=path_folder_LEDET,
        verbose=True)
    simsToRun = {
        'MCSXF': [n + 1200 for n in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]]+[n + 1200 for n in [52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68]],
        # 'MCDXF': [1001],
    }
    # time.sleep(60*60*1.5)
    simsToRun = {'MCSXF': [2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007]}

    finished_sims = {'MCTXF': [], 'MCTSXF': [], 'MCOXF': [], 'MCSXF': [], 'MCDXF': []}
    while True:
        # as long as there is a sim number, try to simulate it (finished sim nums will be deleted)
        while not all(isinstance(value, list) and not value for value in simsToRun.values()):
            for nameMagnet, list_numbers in simsToRun.items():
                for sim in list_numbers:
                    # run simulation
                    if type(sim) == int: sim = str(sim)
                    dLEDET.run_LEDET(nameMagnet, sim, simFileType='.yaml')
                    # delete sim number when mat simulation results have been saved and files are readable
                    mat_output_path = path_folder_LEDET + f'\\{nameMagnet}\\Output\\Mat Files\\SimulationResults_LEDET_{sim}.mat'
                    if os.path.exists(mat_output_path):
                        try:
                            get_signals_from_mat(mat_output_path, ['time_vector'])
                        except Exception as e:
                            warnings.warn(f'{nameMagnet}: Could not read results of simulation {sim}. Rerunning later...')
                            try:
                                # copy input files from shared folder to local folder
                                shared_folder = f'\\\\eosproject-smb\\eos\\project\\s\\steam\\steam_sdk_data\\LEDET\\{nameMagnet}\\Input\\{nameMagnet}_{sim}.yaml'
                                local_folder = f'C:\\tempLEDET\LEDET\{nameMagnet}\Input\{nameMagnet}_{sim}.yaml'
                                shutil.copy(shared_folder, local_folder)
                            except:
                                random_seconds = random.randint(10, 300)
                                print(f'Tried to copy file {shared_folder}. \nWaiting for {random_seconds}sec...')
                                time.sleep(random_seconds)
                            else:
                                print(f'Successfully read simulation {nameMagnet}_{sim}.')
                        else:
                            print(f'Simulation {sim} for magnet {nameMagnet} succeeded.')
                            simsToRun[nameMagnet].remove(int(sim))  # delete finished sim number
                            finished_sims[nameMagnet].append(int(sim))
                            print(f'Simulations that will be run: {simsToRun}')
                            print(f'All finished simulations: {finished_sims}')
                    else:
                        warnings.warn(f'{nameMagnet}: Simulation number {sim} did not succeed. Retrying later...')
                        try:
                            # copy input files from shared folder to local folder
                            shared_folder = f'\\\\eosproject-smb\\eos\\project\\s\\steam\\steam_sdk_data\\LEDET\\{nameMagnet}\\Input\\{nameMagnet}_{sim}.yaml'
                            local_folder = f'C:\\tempLEDET\LEDET\{nameMagnet}\Input\{nameMagnet}_{sim}.yaml'
                            shutil.copy(shared_folder, local_folder)
                        except:
                            random_seconds = random.randint(10, 300)
                            print(f'Tried to copy file for {nameMagnet}_{sim}. Waiting for {random_seconds}sec...')
                            time.sleep(random_seconds)
                        else:
                            print(f'Successfully read simulation {nameMagnet}_{sim}.')
