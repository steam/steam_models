from steam_sdk.analyses.AnalysisSTEAM import AnalysisSTEAM

if __name__ == "__main__":
    aSTEAM = AnalysisSTEAM(file_name_analysis='analysis_run_ref_model.yaml', verbose=True).run_analysis()

