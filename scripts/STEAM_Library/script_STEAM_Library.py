import os
from pathlib import Path
from steam_sdk.builders.BuilderModel import BuilderModel

# Move to the root folder
initial_file_folder = os.path.dirname(__file__)
initial_file_folder_split = os.path.abspath(initial_file_folder).split(os.sep)
print('Initial working folder: {}'.format(initial_file_folder))

if initial_file_folder_split[-1] == 'STEAM_Library' and initial_file_folder_split[-2] == 'scripts':
    path_two_levels_up = Path(Path(initial_file_folder).parent).parent
    os.chdir(path_two_levels_up)
    print('Moved to new working folder: {}'.format(os.getcwd()))


# Define inputs
case_model:    str  = 'magnet'
model_name:    str  = 'MCSXF'
software:      str  = 'LEDET'
flagBuild:     bool = True
verbose:       bool = True
flag_plot_all: bool = True
relative_path_settings = Path('settings', 'user_settings').resolve()

print('case_model:    {}'.format(case_model))
print('model_name:    {}'.format(model_name))
print('software:      {}'.format(software))
print('flagBuild:     {}'.format(flagBuild))
print('verbose:       {}'.format(verbose))
print('flag_plot_all: {}'.format(flag_plot_all))
print('relative_path_settings: {}'.format(relative_path_settings))


# If the output folder does not exist, make it
output_folder_parent = os.path.join(case_model +'s', model_name, 'output')
if not os.path.isdir(output_folder_parent):
    print("Output folder {} does not exist. Making it now".format(output_folder_parent))
    Path(output_folder_parent).mkdir(parents=True)

file_model_data = os.path.join(case_model +'s', model_name, 'input', 'modelData_' + model_name + '.yaml')
output_folder   = os.path.join(case_model +'s', model_name, 'output', software)


# Build model
print('Model generation started.')
BM = BuilderModel(file_model_data=file_model_data, case_model=case_model, software=[software], flag_build=flagBuild,
                  output_path=output_folder, verbose=verbose, flag_plot_all=flag_plot_all, relative_path_settings=relative_path_settings )
print('Model generation completed.')
