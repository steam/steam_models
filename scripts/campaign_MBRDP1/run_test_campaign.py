from steam_sdk.analyses.AnalysisSTEAM import AnalysisSTEAM

if __name__ == "__main__":
    file_name_analysis = 'analysisSTEAM_campaign_MBRDP1.yaml'

    aSTEAM = AnalysisSTEAM(file_name_analysis=file_name_analysis, verbose=True).run_analysis()
