import os
from steam_sdk.viewers.Viewer import Viewer

if __name__ == "__main__":
    name_analysis = 'MBRDP1'
    file_name_transients = f'{name_analysis}.csv'

    V = Viewer(file_name_transients,
               verbose=True,
               list_events=[],  # if list_events is empty it reads all the rows in the file
               flag_display=False,
               flag_save_figures=True,
               figure_types=['png', 'svg', 'pdf'],
               path_output_html_report=os.path.join('output', 'figures', 'MBRDP1.html')
               )
