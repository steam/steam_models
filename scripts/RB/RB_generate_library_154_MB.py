# This script is used to automatically generate the library for the models of all MB magnets, including their coil
# resistances, which can be set with individual stimuli


def add_library_MB(library_name: str, m: int):
    '''
    Helper function to write the model of one specific MB magnet, including its coil resistances
    :param library_name: name of the library file that to be written
    :param m: magnet number
    :return: None
    '''
    model_entry = [
        '*$************************************************************************************',
        f'.subckt MB_Dipole_withCoilRes_M{m} (1_pIn 1_pMid 1_pOut 1_pGND)',
        '+ PARAMS:',
        '+ L_mag = 98e-3',
        '+ C_mag_gnd  = 300e-9',
        '+ R_parallel = 100',
        '+ K     = 0.75',
        '+ R1    = 10',
        '+ R2    = 10',
        '+ RGnd1 = 11e06',
        '+ RGnd2 = 11e06',
        '+ RGnd3 = 11e06',
        '+ RGnd4 = 11e06',
        ' ',
        '* Fake voltage source to easily access the input current',
        'V_monitor_in (1_pIn 100) 0',
        ' ',
        '* Inductors and voltage sources modeling coil resistances',
        'L1  (100 101)  {(1-K)*L_mag/2}',
        'L1b (101 102r) {K    *L_mag/2}',
        'E_coil_resistance_1 (102r 102) VALUE = {I(L1)*V(1r)}',
        'L2  (102 103)  {(1-K)*L_mag/2}',
        'L2b (103 104r) {K    *L_mag/2}',
        'E_coil_resistance_2 (104r 104) VALUE = {I(L2)*V(2r)}',
        ' ',
        '* Coil resistances of the two magnet halves',
        f'V_r_field_1	(1r 0) STIMULUS = R_coil_1_M{m}',
        f'V_r_field_2	(2r 0) STIMULUS = R_coil_2_M{m}',
        ' ',
        '*Resistors to model non-linear losses in the magnet',
        'r1 (101 102r) {R1}',
        'r2 (103 104r) {R2}',
        ' ',
        '* Midport for picking up voltage across each aperture',
        'v1_bbMid_PH (102 1_pMid) 0',
        ' ',
        '* Resistor in parallel (real component)',
        'r_parallel (100 104) {R_parallel}',
        ' ',
        '* Resistors to GND',
        'rGnd1   (100 1_pGND) {RGnd1}',
        'rGnd2_3 (102 1_pGND) {RGnd2*RGnd3/(RGnd2+RGnd3)}',
        'rGnd4   (104 1_pGND) {RGnd4}',
        ' ',
        '* Capacitors to GND',
        'c1        (100 1_pGND) {C_mag_gnd/4}',
        'c2_3      (102 1_pGND) {C_mag_gnd/2}',
        'c4        (104 1_pGND) {C_mag_gnd/4}',
        ' ',
        '* Fake voltage source to easily access the output current',
        'V_monitor_out (104 1_pOut) 0',
        '.ends',
        '*$************************************************************************************',
        ' ',
    ]

    with open(library_name, 'a') as out:
        for row in model_entry:
            out.write(row + '\n')


if __name__ == "__main__":
    ################################ Define inputs ################################
    N_MAG = 154
    library_name = 'RB_MB_154_magnets.lib'

    ################################ Write library header ################################
    library_header = [
        '* PSPICE LHC RB circuit magnet (MB) components library',
        '* Version 1.0: 2015/09/17, Lorenzo Bortot, STEAM, TE-MPE-PE, CERN, Geneva, CH',
        '* Version 2.0: 2022/05/29, Emmanuele Ravaioli, STEAM, TE-MPE-PE, CERN, Geneva, CH',
        '* - This model is similar to RB_MB_Dipole but no internal protection diode',
        '* Version 2.1: 2022/06/14, Emmanuele Ravaioli, STEAM, TE-MPE-PE, CERN, Geneva, CH',
        '* - Coil resistances of the two magnet halves are included in the model and controlled with dedicated stimuli.',
        '*   Since all stimuli must have different name, individual models will be generated with different stimulus name.',
        '*   Note: In PSPICE, if a simulus is not defined its values are defaulted to zeros for the entire simulation.',
        ' ',
    ]
    with open(library_name, 'w') as out:
        for row in library_header:
            out.write(row + '\n')

    ################################ Write the library entries ################################
    for m in range(N_MAG):
        add_library_MB(library_name, m + 1)

# This is the expected output, for m=1
'''
* PSPICE LHC RB circuit magnet (MB) components library
* Version 1.0: 2015/09/17, Lorenzo Bortot, STEAM, TE-MPE-PE, CERN, Geneva, CH
* Version 2.0: 2022/05/29, Emmanuele Ravaioli, STEAM, TE-MPE-PE, CERN, Geneva, CH
* - This model is similar to RB_MB_Dipole but no internal protection diode
* Version 2.1: 2022/06/14, Emmanuele Ravaioli, STEAM, TE-MPE-PE, CERN, Geneva, CH
* - Coil resistances of the two magnet halves are included in the model and controlled with dedicated stimuli.
*   Since all stimuli must have different name, individual models will be generated with different stimulus name.
*   Note: In PSPICE, if a simulus is not defined its values are defaulted to zeros for the entire simulation.


*$************************************************************************************
.subckt MB_Dipole_withCoilRes_M1 (1_pIn 1_pMid 1_pOut 1_pGND)
+ PARAMS:
+ L_mag = 98e-3
+ C_mag_gnd  = 300e-9
+ R_parallel = 100
+ K     = 0.75
+ R1    = 10
+ R2    = 10
+ RGnd1 = 11e06
+ RGnd2 = 11e06
+ RGnd3 = 11e06
+ RGnd4 = 11e06

* Fake voltage source to easily access the input current
V_monitor_in (1_pIn 100) 0

* Inductors and voltage sources modeling coil resistances 
L1  (100 101)  {(1-K)*L_mag/2}
L1b (101 102r) {K    *L_mag/2}
E_coil_resistance_1 (102r 102) VALUE = {I(L1)*V(1r)}
L2  (102 103)  {(1-K)*L_mag/2}
L2b (103 104r) {K    *L_mag/2}
E_coil_resistance_2 (104r 104) VALUE = {I(L2)*V(2r)}

* Coil resistances of the two magnet halves
V_r_field_1	(1r 0) STIMULUS = R_coil_1_M1
V_r_field_2	(2r 0) STIMULUS = R_coil_2_M1

*Resistors to model non-linear losses in the magnet
r1 (101 102r) {R1}
r2 (103 104r) {R2}

* Midport for picking up voltage across each aperture
v1_bbMid_PH (102 1_pMid) 0

* Resistor in parallel (real component)
r_parallel (100 104) {R_parallel}

* Resistors to GND
rGnd1   (100 1_pGND) {RGnd1}
rGnd2_3 (102 1_pGND) {RGnd2*RGnd3/(RGnd2+RGnd3)}
rGnd4   (104 1_pGND) {RGnd4}

* Capacitors to GND
c1        (100 1_pGND) {C_mag_gnd/4}
c2_3      (102 1_pGND) {C_mag_gnd/2}
c4        (104 1_pGND) {C_mag_gnd/4}

* Fake voltage source to easily access the output current
V_monitor_out (104 1_pOut) 0
.ends
'''