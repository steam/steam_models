import yaml
import h5py
import matplotlib.pyplot as plt
import numpy as np
import mplcursors
from itertools import chain

from steam_sdk.data.DataModelMagnet import DataModelMagnet


# INSERT MAGNET NAME HERE
# mag_name = 'MCTSXF'

for mag_name in ['MCDXF']:
    # Read modelData as a dictionary and instantiate DataModelMagnet class
    path = f'../../../magnets/{mag_name}/input/modelData_{mag_name}.yaml'
    with open(path, 'r') as file:
        config = yaml.load(file, Loader=yaml.FullLoader)
    magnet = DataModelMagnet(**config)

    # extract needed parameters from DataModelMagnet class
    n_halfturns = len(magnet.Options_LEDET.quench_initiation.iStartQuench)
    n_steam_groups = len(magnet.CoilWindings.conductor_to_group)
    cables_per_ribbon = magnet.Conductors[0].cable.n_strands
    ribbons_per_halfpole = int(n_halfturns/n_steam_groups)
    print(f'Parameters for the selected magnet {mag_name}:')
    print(f' - n_halfturns = {n_halfturns}')
    print(f' - n_steam_groups = {n_steam_groups}')
    print(f' - cables_per_ribbon = {cables_per_ribbon}')
    print(f' - ribbons_per_halfpole = {ribbons_per_halfpole}')

    # coil indices
    coil_indices = [list(range(i*cables_per_ribbon*ribbons_per_halfpole, (i+1)*cables_per_ribbon*ribbons_per_halfpole)) for i in range(int(n_halfturns/(cables_per_ribbon*ribbons_per_halfpole*2)))]
    coil_indices = [list+[int(val+n_halfturns/2) for val in list] for list in coil_indices]
    print(coil_indices)

    # calculate groups to coils
    print('groups_to_coils:\n')
    first = list(range(1, cables_per_ribbon+1)) + [i+int(n_steam_groups/2) for i in range(1, cables_per_ribbon+1)]
    for i in range(int(n_steam_groups/cables_per_ribbon/2)):
        print([num + i*cables_per_ribbon for num in first])

    # calculate group together
    first = [[int(n_steam_groups/2)+1+i, 1+i] for i in range(cables_per_ribbon)]
    second = [[x + cables_per_ribbon for x in reversed(sublist)] for sublist in reversed(first)] # add 12 and reverse list and nested list
    first.extend(second)
    group_together = []
    for i in range(int(n_steam_groups/cables_per_ribbon/4)):
        group_together.extend([[x + 2*i*cables_per_ribbon for x in sublist] for sublist in first])
    print(f'group_together: {group_together}')


    # print bare cable dimensions (von ausen nach innen)
    bare_cable_width_min = 0.0004526166139912186
    bare_cable_width_max = 0.000601667
    bare_cable_width_linear = [list(np.linspace(bare_cable_width_max, bare_cable_width_min, 12))] * int(n_steam_groups/12)
    bare_cable_width_linear = list(chain(*bare_cable_width_linear))  # flatten
    print(f'bare_cable_width_linear: {bare_cable_width_linear}')
    bare_cable_height_min = 0.00043380983989561346
    bare_cable_height_max = 0.000576667
    bare_cable_width_linear = [list(np.linspace(bare_cable_height_max, bare_cable_height_min, 12))] * int(n_steam_groups/12)
    bare_cable_width_linear = list(chain(*bare_cable_width_linear))  # flatten
    print(f'bare_cable_width_linear: {bare_cable_width_linear}')

    # calculate cooling values
    if len(magnet.Conductors) == 1:
        insulation_along_height = magnet.Conductors[0].cable.th_insulation_along_height
        insulation_along_width = magnet.Conductors[0].cable.th_insulation_along_width
    else: raise Exception('Case of more then one Conductor not covered in the code')
    # thickness_aperture, thickness_outside, thickness_left, thickness_right = [], [], [], []  #TODO NOTE: This code is using the proposed new entry - to be discussed
    # for scheme_of_group in magnet.CoilWindings.ground_insulations.schemes_to_group:
    #     thickness_aperture.append(magnet.CoilWindings.ground_insulations.schemes[scheme_of_group-1].insulation_towards_aperture)
    #     thickness_outside.append(magnet.CoilWindings.ground_insulations.schemes[scheme_of_group-1].insulation_towards_outside)
    #     thickness_left.append(magnet.CoilWindings.ground_insulations.schemes[scheme_of_group-1].inner_side_insulation)
    #     thickness_right.append(magnet.CoilWindings.ground_insulations.schemes[scheme_of_group-1].outer_side_insulation)
    # dict_thickness = {
    #     'insulation towards aperture':
    #         [(insulation_along_height)/(th+insulation_along_height) for th in thickness_aperture],  # blue
    #     'insulation towards outside':
    #         [(insulation_along_height)/(th+insulation_along_height) for th in thickness_outside],   # red
    #     'inner side insulation':
    #         [(insulation_along_width)/(th+insulation_along_width) for th in thickness_left],        # green
    #     'outer side insulation':
    #         [(insulation_along_width)/(th+insulation_along_width) for th in thickness_right],       # yellow
    # }
    dict_thickness = {
        'insulation towards aperture': (insulation_along_height)/(0.0015+insulation_along_height),  # blue
        'insulation towards outside': (insulation_along_height)/(0.001+insulation_along_height),    # red
        'inner side insulation': (insulation_along_width)/(0.00015 +insulation_along_width),         # green
        'outer side insulation': (insulation_along_width)/(0.001+insulation_along_width),           # yellow
    }

    # index of insulation towards aperture
    plot_mask_aperture = [ribbons_per_halfpole-1+(ribbons_per_halfpole*i) for i in range(n_steam_groups)]
    # index of insulation towards outside
    plot_mask_outside = [ribbons_per_halfpole*i for i in range(n_steam_groups)]
    # index of outer side insulation
    arr_outer_side = np.arange(ribbons_per_halfpole)
    plot_mask_outer_side = np.concatenate([arr_outer_side + ribbons_per_halfpole*cables_per_ribbon*i for i in range(int(n_steam_groups/cables_per_ribbon))])
    # index of inner side insulation
    plot_mask_inner_side = [val + ribbons_per_halfpole * (cables_per_ribbon - 1) for val in plot_mask_outer_side]

    # make coil connection plot mask
    n_poles = int(n_halfturns/(2*cables_per_ribbon*ribbons_per_halfpole))
    plot_mask_coil_connection = [(cables_per_ribbon*ribbons_per_halfpole)*(i+1)-1 for i in range(n_poles)] \
                                + [int(n_halfturns/2)+ribbons_per_halfpole+(cables_per_ribbon*ribbons_per_halfpole)*i-1 for i in range(n_poles)]
    plot_masks_quench_locations_31locations = {
        'MCSXF': [1752, 1793, 105, 1898, 1957, 576, 516, 457, 2121, 2081, 2328, 2369, 681, 2474, 804, 864, 1092, 2762, 969, 2657, 2616, 2945, 2985, 1321, 1380, 1440, 3397, 3338, 1545, 3233, 3192],
        'MCOXF': [3007, 3072, 3186, 3271, 3694, 615, 3484, 419, 3768, 880, 3964, 1090, 1410, 1327, 1211, 4123, 1583, 1699, 1782, 2206, 5080, 1996, 4884, 2279, 5344, 2475, 5554, 5875, 5790, 5676, 5611],
        'MCDXF': [2299, 2354, 2430, 456, 2657, 2581, 2755, 529, 605, 912, 3113, 757, 3211, 985, 1061, 1368, 1289, 1213, 3439, 1441, 1517, 1596, 1745, 1669, 3895, 4177, 4253, 2052, 4482, 4406, 4351],
        'MCTSXF': [37, 5493, 349, 6120, 611, 6253, 1054, 6565, 1680, 6826, 7141, 1942, 7453, 2568, 7714, 7585, 8158, 3012, 8785, 3274, 8473, 9046, 3900, 9673, 4162, 9361, 4607, 10116, 5233, 10377, 10249],
        'MCTSXF_27': [37, 195, 5736, 6035, 5835, 6345, 1222, 6994, 6788, 7168, 2039, 7919, 7743, 7585, 8187, 8363, 3371, 8500, 9008, 9214, 4330, 9453, 9831, 10031, 10620, 5079, 10249],
        'MCTXF': [37, 5339, 52, 108, 82, 130, 178, 152, 208, 5575, 5589, 287, 5644, 340, 5695, 5712, 420, 6214, 6187, 834, 805, 6107, 766, 6054, 671, 697, 5975, 5929, 5955, 5897, 5859, 5845, 491, 461, 921, 6223, 6273, 992, 966, 1022, 1062, 1074, 1100, 6459, 1153, 1179, 6528, 6561, 1295, 6596, 6641, 1769, 7071, 7054, 1689, 6959, 1658, 6938, 1563, 6917, 6859, 6821, 6807, 6781, 6751, 6729, 1375, 1353, 1805, 7107, 7165, 1876, 1888, 1914, 1946, 1966, 7321, 7343, 2045, 7400, 2084, 7453, 2179, 2152, 7533, 2653, 2592, 7946, 7901, 2524, 7878, 2493, 2455, 7769, 7743, 2386, 2362, 2336, 7643, 7613, 7587, 2245, 2689, 8031, 8057, 8087, 2780, 2806, 2830, 8187, 8213, 2899, 2937, 8322, 2968, 8345, 8390, 3036, 3097, 8865, 3484, 3511, 8785, 3416, 8732, 3377, 8675, 8653, 3298, 3278, 3246, 3220, 3208, 8497, 8439, 3137, 3573, 3595, 8949, 8971, 9001, 9027, 9041, 9079, 9137, 3783, 9158, 3878, 9179, 3909, 9274, 9291, 9318, 9749, 9704, 4403, 4340, 9636, 4287, 4261, 9567, 4208, 4182, 4170, 4130, 4074, 4100, 9381, 9331, 4029, 4457, 4487, 9841, 9855, 9893, 9951, 9925, 9971, 4693, 4667, 10050, 4762, 10103, 4801, 4830, 10183, 10210, 5304, 10596, 10579, 5224, 10528, 5171, 10473, 10459, 5092, 5036, 5062, 5014, 4966, 4992, 4936, 10223, 4921],
        'MCTXF_27': [37, 203, 410, 6043, 5839, 1023, 1228, 7000, 6795, 7172, 2047, 2629, 7751, 7585, 8195, 3073, 3379, 8504, 9015, 9220, 4336, 4131, 9835, 10039, 5294, 5087, 4921],
    }

    # Read model geometry from .mat file
    sim_folder = '\\\eosproject-smb\eos\project\s\steam\steam_sdk_data\LEDET'
    path_file_model_geometry = sim_folder + f'\\{mag_name}\Output\Mat Files\SimulationResults_LEDET_0.mat'
    with h5py.File(path_file_model_geometry, 'r') as simulationSignals:
        XY_in_mm = np.array(simulationSignals['model_geometry']['XY_MAG_ave'])  # in [mm]
    x_conductors, y_conductors = XY_in_mm[:][0]/1000, XY_in_mm[:][1]/1000  # in [m]

    # Plot conductor positions in the coil cross-section
    fig, ax = plt.subplots()
    # plt.scatter(x_conductors, y_conductors, s=10, c='grey', marker='o', label='conductors')
    colors = ['red', 'green', 'blue', 'yellow', 'orange', 'purple', 'grey', 'cyan', 'magenta', 'black', 'pink', 'brown']
    for idx, list in enumerate(coil_indices):
        for i in list:
            plt.scatter(x_conductors[i], y_conductors[i], s=10, c=colors[idx], marker='o')
    # plt.scatter(x_conductors[plot_mask_aperture], y_conductors[plot_mask_aperture], s=30, c='b', marker=8, label='f_cooling_left')
    # plt.scatter(x_conductors[plot_mask_outside], y_conductors[plot_mask_outside], s=30, c='r', marker=9, label='f_cooling_right')
    # plt.scatter(x_conductors[plot_mask_inner_side], y_conductors[plot_mask_inner_side], s=30, c='g', marker=10, label='f_cooling_up')
    # plt.scatter(x_conductors[plot_mask_outer_side], y_conductors[plot_mask_outer_side], s=30, c='y', marker=11, label='f_cooling_down')
    # for idx in plot_mask_coil_connection:
    #     plt.plot(x_conductors[idx], y_conductors[idx], 'rx', markersize=5)
    # for i, idx in enumerate(plot_masks_quench_locations_31locations[mag_name]):
    #     if i == 0:
    #         plt.plot(x_conductors[idx-1], y_conductors[idx-1], 'rx', markersize=5, label='quench locations')
    #     else: plt.plot(x_conductors[idx-1], y_conductors[idx-1], 'rx', markersize=5)
    def on_hover(sel):
        # Check if the selected artist corresponds to the first scatter plot
        if sel.artist == ax.collections[0]:
            # Display the index of the hovered point
            ind = sel.index + 1
            sel.annotation.set(text=f"Index: {ind}")
        else:
            # Do not display the index for other scatter plots
            sel.annotation.set_visible(False)
    # Enable hovering and set the hover function
    cursor = mplcursors.cursor(ax, hover=True)
    cursor.connect("add", on_hover)

    plt.legend(loc='best')
    plt.xlabel('x [m]')
    plt.ylabel('y [m]')
    plt.xlim([-0.11, 0.11])
    plt.ylim([-0.11, 0.11])
    # plt.xlim([0, 0.11])
    # plt.ylim([0, 0.11])
    plt.title(f'{mag_name} - Conductor positions')
    plt.axis('square')
    plt.grid(which='both')
    plt.show()



# print the sim3D_f_cooling lists
# NOTE: up, down, left, right is defined using the first group on the right and then continued symmetrically
sim3D_f_cooling_left = [dict_thickness['insulation towards aperture'] if i in plot_mask_aperture else 0.0 for i in range(n_halfturns)]
sim3D_f_cooling_right = [dict_thickness['insulation towards outside'] if i in plot_mask_outside else 0.0 for i in range(n_halfturns)]
sim3D_f_cooling_up = [dict_thickness['inner side insulation'] if i in plot_mask_inner_side else 0.0 for i in range(n_halfturns)]
sim3D_f_cooling_down = [dict_thickness['outer side insulation'] if i in plot_mask_outer_side else 0.0 for i in range(n_halfturns)]
# for idx in plot_mask_coil_connection:
#     sim3D_f_cooling_left[idx]  = 0.044585987261146494 # (insulation_along_height)/(0.0+insulation_along_height)
#     sim3D_f_cooling_right[idx] = 0.0 # (insulation_along_height)/(0.0+insulation_along_height)
#     sim3D_f_cooling_up[idx]    = 0.0 # (insulation_along_width) /(0.0+insulation_along_width)
#     sim3D_f_cooling_down[idx]  = 0.06542056074766354 # (insulation_along_width) /(0.0+insulation_along_width)
#     print(f'\nAfter validating the selected Conductors in the plot, paste the following parameters in the modelData of {mag_name}')
print(f'    sim3D_f_cooling_down: {sim3D_f_cooling_down}')
print(f'    sim3D_f_cooling_up: {sim3D_f_cooling_up}')
print(f'    sim3D_f_cooling_left: {sim3D_f_cooling_left}')
print(f'    sim3D_f_cooling_right: {sim3D_f_cooling_right}\n')

print(f'    list_coil_connection_ht_idx: {[i+1 for i in plot_mask_coil_connection]}')




# print the racetrack geometry information
if mag_name == 'MCSXF': index_right_down, index_left_up = 0, 287
elif mag_name == 'MCOXF': index_right_down, index_left_up = 0, 371
elif mag_name == 'MCDXF': index_right_down, index_left_up = 0, 227
elif mag_name == 'MCTXF': index_right_down, index_left_up = 0, 443
elif mag_name == 'MCTSXF': index_right_down, index_left_up = 0, 443
point_right_down = [x_conductors[index_right_down], y_conductors[index_right_down]]
point_left_up = [x_conductors[index_left_up], y_conductors[index_left_up]]
# calc conductor width
help_point = [x_conductors[index_right_down+1], y_conductors[index_right_down+1]]
conductor_width = ((help_point[0] - point_right_down[0])**2 + (help_point[1] - point_right_down[1])**2)**(0.5)
print(f'\nParameters of geometry model for FiQuS:')
print(f'winding_width: {point_right_down[0]-point_left_up[0]+conductor_width}')
print(f'winding_height: {-(point_right_down[1]-point_left_up[1])+conductor_width}')
print(f'inner_height: {-point_left_up[1]-conductor_width/2}')
print(f'placement.radius: {(point_right_down[0]+point_left_up[0])/2}')





import numpy as np

n_strands = 12*24
area = 17.8*8.6 - n_strands*np.pi*(0.25+7e-02)**2
print(f'MCSXF area per strand: {round(area/n_strands, 3)} mm**2, should be {area/280}')

n_strands = 12*31
area = 23.1*7.6 - n_strands*np.pi*(0.25+7e-02)**2
print(f'MCOXF area per strand:  {round(area/n_strands, 3)} mm**2, should be {area/372}')

n_strands = 12*19
area = 14.1*7.7 - n_strands*np.pi*(0.25+7e-02)**2
print(f'MCDXF area per strand:  {round(area/n_strands, 3)} mm**2, should be {area/228}')

n_strands = 12*37
area = 26.7*8 - n_strands*np.pi*(0.25+7e-02)**2
print(f'MCTXF area per strand:  {round(area/n_strands, 3)} mm**2, should be {area/436}')

n_strands = 12*37
area = 26.7*8 - n_strands*np.pi*(0.25+7e-02)**2
print(f'MCTSXF area per strand: {round(area/n_strands, 3)} mm**2, should be {area/436}')
