# function to print out all CoilWinding entries for multipole magnets
def print_CoilWindings_entries(r_groups, cbr, s_groups, ml, turns):
    # conductor_to_group
    conductor_to_group = s_groups * [1]
    print('conductor_to_group: ' + str(conductor_to_group))
    print('- length = ' + str(len(conductor_to_group)))

    # group_to_coil_section
    group_to_coil_section = s_groups * [1]
    print('group_to_coil_section: ' + str(group_to_coil_section))
    print('- length = ' + str(len(group_to_coil_section)))

    # group_to_coil_section
    polarities_in_group = int(r_groups / 4) * (cbr * [+1] + cbr * [-1]) + int(r_groups / 4) * (cbr * [-1] + cbr * [+1])
    print('polarities_in_group: ' + str(polarities_in_group))
    print('- length = ' + str(len(polarities_in_group)))

    # half_turn_length
    half_turn_length = s_groups * [ml]
    print('half_turn_length: ' + str(half_turn_length))
    print('- length = ' + str(len(half_turn_length)))

    # overwrite_electrical_order
    overwrite_electrical_order = list(range(1, turns + 1))
    print('overwrite_electrical_order: ' + str(overwrite_electrical_order))
    print('- length = ' + str(len(overwrite_electrical_order)))

    # alphaDEG_ht
    alphaDEG_ht = turns * [0]
    print('alphaDEG_ht: ' + str(alphaDEG_ht))
    print('- length = ' + str(len(alphaDEG_ht)))

    # iStartQuench
    print('iStartQuench: ' + str(overwrite_electrical_order))
    print('- length = ' + str(len(overwrite_electrical_order)))

    # tStartQuench
    tStartQuench = turns * [99999.0]
    print('tStartQuench: ' + str(tStartQuench))
    print('- length = ' + str(len(tStartQuench)))

    # lengthHotSpot_iStartQuench
    lengthHotSpot_iStartQuench = turns * [0.0]
    print('lengthHotSpot_iStartQuench: ' + str(lengthHotSpot_iStartQuench))
    print('- length = ' + str(len(lengthHotSpot_iStartQuench)))

    # fScaling_vQ_iStartQuench
    fScaling_vQ_iStartQuench = turns * [1.0]
    print('fScaling_vQ_iStartQuench: ' + str(fScaling_vQ_iStartQuench))
    print('- length = ' + str(len(fScaling_vQ_iStartQuench)))

def print_group_together(number_of_groups, ribbons_per_cable, verbose=False):
    group_list = []
    for i in range(1, int(number_of_groups/2)+1):
        if int((i-1)/(ribbons_per_cable)) % 2 == 0:
            group_list.append([i, i + int(number_of_groups / 2)])
        else:
            group_list.append([i + int(number_of_groups / 2), i])

    if verbose:
        print('group_together entries sorted with newline after every coil for faster checking')
        for j in range(0, int(len(group_list)/ribbons_per_cable)):
            print(group_list[j*ribbons_per_cable:j*ribbons_per_cable+ribbons_per_cable])
    print('group_together: ' + str(group_list))
    print('reversed: ' + str([int(i%2 != 0) for i in range(len(group_list))]))



# insert Magnet Data here and select Magnet
Magnet = 'MCDXF'  # 'MCTSXF'  'MCTXF'
if Magnet == 'MCDXF':
    calbles_per_ribbon = 12
    roxie_groups = 20
    conductors_per_roxie_group = 19
    mag_len = 0.145
elif Magnet == 'MCTSXF':
    calbles_per_ribbon = 12
    roxie_groups = 24
    conductors_per_roxie_group = 37
    mag_len = 0.099
elif Magnet == 'MCTXF':
    calbles_per_ribbon = 12
    roxie_groups = 24
    conductors_per_roxie_group = 37
    mag_len = 0.469
else:
    raise Exception('Provide an existing magnet name or add you magnet.')


# run print functions
steam_groups = roxie_groups * calbles_per_ribbon  # in steam
number_of_turns = steam_groups * conductors_per_roxie_group
print_CoilWindings_entries(roxie_groups, calbles_per_ribbon, steam_groups, mag_len, number_of_turns)
print_group_together(steam_groups, calbles_per_ribbon, verbose=False)
