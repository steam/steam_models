import h5py
import matplotlib.pyplot as plt
import numpy as np
import csv
import mplcursors

# INSERT MAGNET NAME HERE
mag_name = 'MCSXF'

# Read model geometry from .mat file
sim_folder = '\\\eosproject-smb\eos\project\s\steam\steam_sdk_data\LEDET'
path_file_model_geometry = sim_folder + f'\\{mag_name}\Output\Mat Files\SimulationResults_LEDET_0.mat'
with h5py.File(path_file_model_geometry, 'r') as simulationSignals:
    XY_in_mm = np.array(simulationSignals['model_geometry']['XY_MAG_ave'])  # in [mm]
x_conductors, y_conductors = XY_in_mm[:][0]/1000, XY_in_mm[:][1]/1000  # in [m]

# read generated file from fiqus
data = {}
with open(f'RoxieCrossSection{mag_name}.csv', mode='r') as file:
    reader = csv.DictReader(file)
    for row in reader:
        index = int(row['index'])
        x_pos = float(row['x-pos'])
        y_pos = float(row['y-pos'])
        data[index] = [x_pos, y_pos]

# prepare data
x_conductors = np.array(x_conductors)
y_conductors = np.array(y_conductors)
data_keys = np.array(list(data.keys()))
data_values = np.array(list(data.values()))
x_data, y_data = data_values[:, 0], data_values[:, 1]

# make plot
fig, ax = plt.subplots()
ax.scatter(x_conductors, y_conductors, s=5, c='grey', marker='o', label='Conductors')
ax.scatter(x_data, y_data, s=5, c='b', marker='x', label='Data')
plt.xlabel('x [m]')
plt.ylabel('y [m]')
plt.title(f'{mag_name} - Conductor positions')
plt.legend(loc='best')
plt.axis('square')
plt.grid(which='both')
cursor = mplcursors.cursor(ax, hover=True)
@cursor.connect("add")
def on_add(sel):
    if sel.artist.get_label() == 'Conductors':
        sel.annotation.set_text(str(sel.index+1))
    elif sel.artist.get_label() == 'Data':
        sel.annotation.set_text(str(data_keys[sel.index]))
plt.show()

