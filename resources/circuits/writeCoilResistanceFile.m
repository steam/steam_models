function [] = writeCoilResistanceFile(file_list, outputfolder, time_offset, newLabelMagnet)
    if nargin<3
        time_offset=NaN;
    end
    if nargin<4
        newLabelMagnet='';
    end
    
    n_files=length(file_list);

    header_1=[];
    header_2=[];
    variablesToWrite=[];
    for fil=1:n_files
        file_name = file_list{fil};
        display([num2str(fil) '/' num2str(n_files) ': ' file_name])

        %% Acquire data
        load(file_name)

        if strcmp(newLabelMagnet,'')
            fileCsvOutput=['interpolation_resistance_' nameMagnet '.csv'];
        else
            fileCsvOutput=['interpolation_resistance_' newLabelMagnet '.csv'];
        end

        if isnan(time_offset)==1
            time_offset = t_PC;
            display(['The PC switching-off time is ' num2str(t_PC) ' s.'])
        else
            display(['The manually defined time offset is ' num2str(time_offset) ' s.'])
        end
        display(['The number of time points is ' num2str(n_time) '.'])

        if size(R_CoilSections,2)>1
            error('There are more than 1 coil sections. Let us talk :)')
        end

        %% Prepare variables to write
        header_1 = [header_1 num2str(Ia(1)) ',,,'];
        header_2 = [header_2 'time_vector,R_CoilSections_1,,'];

        %% Check time vector length and if needed correct matrix
        if length(time_vector)>size(variablesToWrite,1)
            new_variablesToWrite=NaN*zeros(length(time_vector),size(variablesToWrite,2)+3);
            if fil>1
                rowsToWrite = 1:size(variablesToWrite,1);
                colsToWrite = 1:size(variablesToWrite,2);
                new_variablesToWrite(rowsToWrite,colsToWrite)=variablesToWrite(rowsToWrite,colsToWrite);
            end
            variablesToWrite=new_variablesToWrite;
            clear new_variablesToWrite
        end    
        variablesToWrite(1:size(R_CoilSections,1),(fil-1)*3+1:(fil-1)*3+2) = [time_vector'-time_offset, R_CoilSections];
    end

    %% Add a column with I=0 A
    header_1 = [header_1 num2str(0) ','];
    header_2 = [header_2 'time_vector,R_CoilSections_1'];
    variablesToWrite(:,(n_files+1-1)*3+1:(n_files+1-1)*3+2) = [time_vector'-time_offset, zeros(size(R_CoilSections))];

    %% Write file
    path_file = fullfile(outputfolder,fileCsvOutput);
    fileID = fopen(path_file, 'w');
    try
        fprintf(fileID,'%s\n', header_1); % Header 1
        fprintf(fileID,'%s\n', header_2); % Header 2
        for s=1:size(variablesToWrite,1)
            for fil=1:n_files+1 % +1 because we also write the case I=0 A
                if sum(isnan(variablesToWrite(s,(fil-1)*3+1:(fil-1)*3+2)))>0
                    fprintf(fileID,'%s', ',');  % this will write only commas (this file format requires this odd formatting)
                else
                    fprintf(fileID,'%9.10e,%9.10e', variablesToWrite(s,(fil-1)*3+1:(fil-1)*3+2));
%                     fprintf(fileID,'%4.3f,%3.2e', variablesToWrite(s,(fil-1)*3+1:(fil-1)*3+2)); % this alternative format could be useful for debugging purposes
                end
                if fil~=n_files+1 % +1 because we also write the case I=0 A
                    fprintf(fileID,',,');
                else
                    fprintf(fileID,'\n');
                end
            end
        end
        fclose(fileID);
    catch
        warning(['Error while trying to write file ' fileCsvOutput '.'])
        fclose(fileID); % close the file anyway
    end
    display(['File ' path_file ' written.'])
end